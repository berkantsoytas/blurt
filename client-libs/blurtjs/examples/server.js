const blurt = require("../lib");

blurt.api.getAccountCount((err, result) => {
    console.log(err, result);
});

blurt.api.getAccounts(["blurtofficial"], (err, result) => {
    console.log(err, result);
    const reputation = blurt.formatter.reputation(result[0].reputation);
    console.log(reputation);
});

blurt.api.getState("trending/blurt", (err, result) => {
    console.log(err, result);
});

blurt.api.getFollowing("blurtofficial", 0, "blog", 10, (err, result) => {
    console.log(err, result);
});

blurt.api.getFollowers("blurtofficial", 0, "blog", 10, (err, result) => {
    console.log(err, result);
});

blurt.api.streamOperations((err, result) => {
    console.log(err, result);
});

blurt.api.getDiscussionsByActive(
    {
        limit: 10,
        start_author: "thecastle",
        start_permlink: "this-week-in-level-design-1-22-2017",
    },
    (err, result) => {
        console.log(err, result);
    }
);
