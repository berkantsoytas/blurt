# 0.7.3 Upgrade Procedure

Before upgrading to 0.7.2, make sure you have done the Hotfix-20220707 upgrade first.

## For Intel-based Witness Nodes (VPS)

```
docker pull registry.gitlab.com/blurt/blurt/witness:0.7.3
```

```
docker stop blurtd
```

```
docker rm blurtd
```

```
docker run -d --net=host -v blurtd:/blurtd --name blurtd registry.gitlab.com/blurt/blurt/witness:0.7.3 /usr/bin/blurtd --data-dir /blurtd  --plugin "witness account_by_key account_by_key_api condenser_api database_api network_broadcast_api transaction_status transaction_status_api rc_api" --webserver-http-endpoint 127.0.0.1:8091 --webserver-ws-endpoint 127.0.0.1:8090 --p2p-endpoint 0.0.0.0:1776
```

## For ARM-based Witness Nodes (Raspberry Pi, Mac M1, etc.)

```
docker pull registry.gitlab.com/blurt/blurt/megadrive:0.7.3
```

```
docker stop blurtd
```

```
docker rm blurtd
```

```
docker run -d --net=host -v blurtd:/blurtd --name blurtd registry.gitlab.com/blurt/blurt/megadrive:0.7.3 /usr/bin/blurtd --data-dir /blurtd  --plugin "witness account_by_key account_by_key_api condenser_api database_api network_broadcast_api transaction_status transaction_status_api rc_api" --webserver-http-endpoint 127.0.0.1:8091 --webserver-ws-endpoint 127.0.0.1:8090 --p2p-endpoint 0.0.0.0:1776
```

## For RPC Nodes

```
docker pull registry.gitlab.com/blurt/blurt/rpc:0.7.3
```

```
docker stop blurtrpc
```

```
docker rm blurtrpc
```

```
docker run -d --net=host -v blurtd:/blurtd --name blurtrpc registry.gitlab.com/blurt/blurt/rpc:0.7.3 /usr/bin/blurtd --data-dir /blurtd --webserver-http-endpoint 0.0.0.0:8091 --webserver-ws-endpoint 0.0.0.0:8090 --p2p-endpoint 0.0.0.0:1776
```
