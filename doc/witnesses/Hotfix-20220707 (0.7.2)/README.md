# Witness Node Hotfix-20220707 Upgrade Procedure

## Backup `config.ini` and `wallet.json`

Change to your home directory and copy the files from your blurtd volume:

```
cd ~
cp /var/lib/docker/volumes/blurtd/_data/config.ini .
cp /var/lib/docker/volumes/blurtd/_data/wallet.json .
```

## Remove Witness Container, Volume, and Images

First remove the container:

```
docker rm -f blurtd
```

Next, remove the volume (this is why we made a backup of `config.ini` and `wallet.json`):

```
docker volume rm blurtd
```

Next, clean up the images in order to be sure to have enough space to download the new one:

```
docker image prune
```

## Download and Install the Hotfix

First pull the image from GitLab:

```
docker pull registry.gitlab.com/blurt/blurt/witness-presync:hotfix-20220707
```

Then run the image:

```
docker run -d --net=host -v blurtd:/blurtd --name blurtd registry.gitlab.com/blurt/blurt/witness-presync:hotfix-20220707 /usr/bin/blurtd --data-dir /blurtd  --plugin "witness account_by_key account_by_key_api condenser_api database_api network_broadcast_api transaction_status transaction_status_api rc_api" --webserver-http-endpoint 127.0.0.1:8091 --webserver-ws-endpoint 127.0.0.1:8090 --p2p-endpoint 0.0.0.0:1776
```

Check the logs to make sure it's running:

```
docker logs blurtd -f
```

## Copy `config.ini` and `wallet.json` and Restart

Copy `wallet.json` and the edited `config.ini` to the `blurtd` volume:

```
cd ~
cp config.ini /var/lib/docker/volumes/blurtd/_data/
cp wallet.json /var/lib/docker/volumes/blurtd/_data/
```

Restart the node:

```
docker restart blurtd
```
